# Ravn GraphQL Code Challenge

Create a web or mobile app that allows you to browse the Star Wars GraphQL Api. The goal of this challenge is to demonstrate your ability to write clean software, communicate clearly, and follow the instructions.

## Getting Started

Server side: A simple express server with GraphQL, this provide a API for retrive people of star Wars.
Font side: A simple application that recovers a list of peoples and a specific people using graphql client and React.

### Prerequisites

You need to install them

```
frontend
"@testing-library/jest-dom": "^4.2.4",
"@testing-library/react": "^9.4.0",
"@testing-library/user-event": "^7.2.1",
"apollo-boost": "^0.4.7",
"apollo-cache-inmemory": "^1.6.5",
"apollo-client": "^2.6.8",
"apollo-link": "^1.2.13",
"apollo-link-rest": "^0.7.3",
"graphql": "^14.5.8",
"graphql-anywhere": "^4.2.6",
"graphql-tag": "^2.10.1",
"node-sass": "^4.13.1",
"qs": "^6.9.1",
"react": "^16.12.0",
"react-apollo": "^3.1.3",
"react-dom": "^16.12.0",
"react-router-dom": "^5.1.2",
"react-scripts": "3.3.0"
backend
"cors": "^2.8.5",
"express": "^4.17.1",
"express-graphql": "^0.9.0",
"graphql": "^14.5.8",
"graphql-tools": "^4.0.6"
```

### Installing

Client

```
cd app
npm install
npm start // dev
npm build
npm test
npm eject
```

Server

```
cd server
npm install
npm start
```

## Running the tests

All tests are found as screenshots in the folder with name 'screenshots' in the main directory

### And coding style tests

```
sass https://sass-lang.com/
```

## Built With

* [Node]
* [React]
* [Express]
* [BabelJs]
* [graphql]
* [apollo]

## Authors

* **Disraeli Ari** - *Initial work* - [Challenger](https://disraely_ari@bitbucket.org/disraely_ari/ravn-challenge-v2.git)

See also the list of [contributors](https://disraely_ari@bitbucket.org/disraely_ari/ravn-challenge-v2.git) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Cap of coffee
* Inspiration
* never give up
