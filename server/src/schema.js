import { makeExecutableSchema } from 'graphql-tools';
import { resolvers } from './resolvers';

const typeDefs = `
  type Query {
    peoples: [People]
    people(name: String!): People
  }

  type People {
    name: String!
    hair_color: String
    skin_color: String
    eye_color: String
    birth_year: String
    gender:  String
    homeworld: Homeworld
    species: [Species]
    vehicles: [Vehicle]
  }

  type Homeworld {
    name: String!
    rotation_period: String
    orbital_period: String
    diameter: String
    climate: String
    gravity: String
    terrain: String
    surface_water: String
    population: String
  }

  type Species {
    name: String!
    classification: String
    designation: String
    average_height: String
    skin_colors: String
    hair_colors: String
    eye_colors: String
    average_lifespan: String
    language: String
  }

  type Vehicle {
    name: String!
    model: String
    manufacturer: String
    cost_in_credits: String
    length: String
    max_atmosphering_speed: String
    crew: String
    passengers: String
    cargo_capacity: String
    consumables: String
    vehicle_class: String
  }
`;

export default makeExecutableSchema({
  typeDefs: typeDefs,
  resolvers: resolvers
})
