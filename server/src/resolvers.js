import { peoples } from './models/people';

export const resolvers = {
  Query: {
    peoples(){
      return peoples;
    },
    people(root, {name}){
      var data = peoples;
      for (var i = 0; i < data.length; i++){
        if (data[i].name == name){
          return data[i];
        }
      }
      return null;
    }
  }
};
