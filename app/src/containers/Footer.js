import React from "react";

function Footer() {
  return (
    <footer>
      <p className="footer-cop">Ravn GraphQL Code Challenge</p>
      <p className="footer-author">disraeli.ar.m@gmail.com</p>
    </footer>
  );
}

export default Footer;
