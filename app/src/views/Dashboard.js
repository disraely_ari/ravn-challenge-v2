import React, { Component } from 'react';
import { gql } from 'apollo-boost';
import { Link } from 'react-router-dom';
import { Query } from 'react-apollo';
import { ReactComponent as Icon} from '../assets/svg/tail-spin.svg';

const peoplesQuery= gql`
  {
    peoples {
      name
      species {
        name
      }
      homeworld {
        name
      }
    }
  }
`;

class Dashboard extends Component {
  render() {
    return (
      <Query query={peoplesQuery}>
      {({ loading, error, data }) => {
          if (loading) return <p className="card-title-loading"> <Icon /> Loading </p>
          if (error){
            return <p className="card-title-failed">Failed to load data</p>;
          }
          return data.peoples.map(people => {
            return (
              <div className="card" key={people.name}>
                <Link to={'/people/'+people.name} className="contentDashboard">
                  <div className="card-body">
                    <div className="people-detail-row">
                      <div className="people-detail-block-name">
                        <p className="card-title">{people.name}</p>
                        <p className="card-subtitle">{people.species[0].name} from {people.homeworld.name}</p>
                      </div>
                      <div className="people-detail-block-value">&#10148;</div>
                    </div>
                  </div>
                </Link>
              </div>);
          })
      }}
      </Query>
    );
  }
}

export default Dashboard;
