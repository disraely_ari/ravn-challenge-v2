import React, { Component } from 'react';
import '../assets/css/index.scss';

class Header extends Component {

  render() {
    return (
      <header>
        <div className="sectionHeader">
          <strong>People of Star Wars</strong>
        </div>
      </header>
    );
  }
}

export default Header;
