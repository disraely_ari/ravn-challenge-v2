export const peoples = [
  {
    "name": "Luke Skywalker",
    "hair_color": "blond",
    "skin_color": "fair",
    "eye_color": "blue",
    "birth_year": "19BBY",
    "gender": "male",
    "homeworld": {
      "name": "Tatooine",
      "rotation_period": "23",
      "orbital_period": "304",
      "diameter": "10465",
      "climate": "arid",
      "gravity": "1 standard",
      "terrain": "desert",
      "surface_water": "1",
      "population": "200000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": [
      {
        "name": "Vulture Droid",
        "model": "Vulture-class droid starfighter",
        "manufacturer": "Haor Chall Engineering, Baktoid Armor Workshop",
        "cost_in_credits": "unknown",
        "length": "3.5",
        "max_atmosphering_speed": "1200",
        "crew": "0",
        "passengers": "0",
        "cargo_capacity": "0",
        "consumables": "none",
        "vehicle_class": "starfighter"
      },
      {
        "name": "Imperial Speeder Bike",
        "model": "74-Z speeder bike",
        "manufacturer": "Aratech Repulsor Company",
        "cost_in_credits": "8000",
        "length": "3",
        "max_atmosphering_speed": "360",
        "crew": "1",
        "passengers": "1",
        "cargo_capacity": "4",
        "consumables": "1 day",
        "vehicle_class": "speeder"
      }
    ]
  },
  {
    "name": "C-3PO",
    "hair_color": "n/a",
    "skin_color": "gold",
    "eye_color": "yellow",
    "birth_year": "112BBY",
    "gender": "n/a",
    "homeworld": {
      "name": "Tatooine",
      "rotation_period": "23",
      "orbital_period": "304",
      "diameter": "10465",
      "climate": "arid",
      "gravity": "1 standard",
      "terrain": "desert",
      "surface_water": "1",
      "population": "200000"
    },
    "species": [
      {
        "name": "Droid",
        "classification": "artificial",
        "designation": "sentient",
        "average_height": "n/a",
        "skin_colors": "n/a",
        "hair_colors": "n/a",
        "eye_colors": "n/a",
        "average_lifespan": "indefinite",
        "language": "n/a",
      }
    ],
    "vehicles": []
  },
  {
    "name": "R2-D2",
    "hair_color": "n/a",
    "skin_color": "white, blue",
    "eye_color": "red",
    "birth_year": "33BBY",
    "gender": "n/a",
    "homeworld": {
      "name": "Naboo",
      "rotation_period": "26",
      "orbital_period": "312",
      "diameter": "12120",
      "climate": "temperate",
      "gravity": "1 standard",
      "terrain": "grassy hills, swamps, forests, mountains",
      "surface_water": "12",
      "population": "4500000000"
    },
    "species": [
      {
        "name": "Droid",
        "classification": "artificial",
        "designation": "sentient",
        "average_height": "n/a",
        "skin_colors": "n/a",
        "hair_colors": "n/a",
        "eye_colors": "n/a",
        "average_lifespan": "indefinite",
        "language": "n/a",
      }
    ],
    "vehicles": []
  },
  {
    "name": "Darth Vader",
    "hair_color": "none",
    "skin_color": "white",
    "eye_color": "yellow",
    "birth_year": "41.9BBY",
    "gender": "male",
    "homeworld": {
      "name": "Tatooine",
      "rotation_period": "23",
      "orbital_period": "304",
      "diameter": "10465",
      "climate": "arid",
      "gravity": "1 standard",
      "terrain": "desert",
      "surface_water": "1",
      "population": "200000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Leia Organa",
    "hair_color": "brown",
    "skin_color": "light",
    "eye_color": "brown",
    "birth_year": "19BBY",
    "gender": "female",
    "homeworld": {
      "name": "Alderaan",
      "rotation_period": "24",
      "orbital_period": "364",
      "diameter": "12500",
      "climate": "temperate",
      "gravity": "1 standard",
      "terrain": "grasslands, mountains",
      "surface_water": "40",
      "population": "2000000000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": [
      {
        "name": "Imperial Speeder Bike",
        "model": "74-Z speeder bike",
        "manufacturer": "Aratech Repulsor Company",
        "cost_in_credits": "8000",
        "length": "3",
        "max_atmosphering_speed": "360",
        "crew": "1",
        "passengers": "1",
        "cargo_capacity": "4",
        "consumables": "1 day",
        "vehicle_class": "speeder"
      }
    ]
  },
  {
    "name": "Owen Lars",
    "hair_color": "brown, grey",
    "skin_color": "light",
    "eye_color": "blue",
    "birth_year": "52BBY",
    "gender": "male",
    "homeworld": {
      "name": "Tatooine",
      "rotation_period": "23",
      "orbital_period": "304",
      "diameter": "10465",
      "climate": "arid",
      "gravity": "1 standard",
      "terrain": "desert",
      "surface_water": "1",
      "population": "200000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Beru Whitesun lars",
    "hair_color": "brown",
    "skin_color": "light",
    "eye_color": "blue",
    "birth_year": "47BBY",
    "gender": "female",
    "homeworld": {
      "name": "Tatooine",
      "rotation_period": "23",
      "orbital_period": "304",
      "diameter": "10465",
      "climate": "arid",
      "gravity": "1 standard",
      "terrain": "desert",
      "surface_water": "1",
      "population": "200000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "R5-D4",
    "hair_color": "n/a",
    "skin_color": "white, red",
    "eye_color": "red",
    "birth_year": "unknown",
    "gender": "n/a",
    "homeworld": {
      "name": "Tatooine",
      "rotation_period": "23",
      "orbital_period": "304",
      "diameter": "10465",
      "climate": "arid",
      "gravity": "1 standard",
      "terrain": "desert",
      "surface_water": "1",
      "population": "200000"
    },
    "species": [
      {
        "name": "Droid",
        "classification": "artificial",
        "designation": "sentient",
        "average_height": "n/a",
        "skin_colors": "n/a",
        "hair_colors": "n/a",
        "eye_colors": "n/a",
        "average_lifespan": "indefinite",
        "homeworld": null,
      }
    ],
    "vehicles": []
  },
  {
    "name": "Biggs Darklighter",
    "hair_color": "black",
    "skin_color": "light",
    "eye_color": "brown",
    "birth_year": "24BBY",
    "gender": "male",
    "homeworld": {
      "name": "Tatooine",
      "rotation_period": "23",
      "orbital_period": "304",
      "diameter": "10465",
      "climate": "arid",
      "gravity": "1 standard",
      "terrain": "desert",
      "surface_water": "1",
      "population": "200000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Obi-Wan Kenobi",
    "hair_color": "auburn, white",
    "skin_color": "fair",
    "eye_color": "blue-gray",
    "birth_year": "57BBY",
    "gender": "male",
    "homeworld": {
      "name": "Stewjon",
      "rotation_period": "unknown",
      "orbital_period": "unknown",
      "diameter": "0",
      "climate": "temperate",
      "gravity": "1 standard",
      "terrain": "grass",
      "surface_water": "unknown",
      "population": "unknown"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": [
      {
        "name": "Tribubble bongo",
        "model": "Tribubble bongo",
        "manufacturer": "Otoh Gunga Bongameken Cooperative",
        "cost_in_credits": "unknown",
        "length": "15",
        "max_atmosphering_speed": "85",
        "crew": "1",
        "passengers": "2",
        "cargo_capacity": "1600",
        "consumables": "unknown",
        "vehicle_class": "submarine"
      }
    ]
  },
  {
    "name": "Anakin Skywalker",
    "hair_color": "blond",
    "skin_color": "fair",
    "eye_color": "blue",
    "birth_year": "41.9BBY",
    "gender": "male",
    "homeworld": {
      "name": "Tatooine",
      "rotation_period": "23",
      "orbital_period": "304",
      "diameter": "10465",
      "climate": "arid",
      "gravity": "1 standard",
      "terrain": "desert",
      "surface_water": "1",
      "population": "200000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": [
      {
        "name": "Zephyr-G swoop bike",
        "model": "Zephyr-G swoop bike",
        "manufacturer": "Mobquet Swoops and Speeders",
        "cost_in_credits": "5750",
        "length": "3.68",
        "max_atmosphering_speed": "350",
        "crew": "1",
        "passengers": "1",
        "cargo_capacity": "200",
        "consumables": "none",
        "vehicle_class": "repulsorcraft"
      },
      {
        "name": "XJ-6 airspeeder",
        "model": "XJ-6 airspeeder",
        "manufacturer": "Narglatch AirTech prefabricated kit",
        "cost_in_credits": "unknown",
        "length": "6.23",
        "max_atmosphering_speed": "720",
        "crew": "1",
        "passengers": "1",
        "cargo_capacity": "unknown",
        "consumables": "unknown",
        "vehicle_class": "airspeeder"
      }
    ]
  },
  {
    "name": "Wilhuff Tarkin",
    "hair_color": "auburn, grey",
    "skin_color": "fair",
    "eye_color": "blue",
    "birth_year": "64BBY",
    "gender": "male",
    "homeworld": {
      "name": "Eriadu",
      "rotation_period": "24",
      "orbital_period": "360",
      "diameter": "13490",
      "climate": "polluted",
      "gravity": "1 standard",
      "terrain": "cityscape",
      "surface_water": "unknown",
      "population": "22000000000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Chewbacca",
    "hair_color": "brown",
    "skin_color": "unknown",
    "eye_color": "blue",
    "birth_year": "200BBY",
    "gender": "male",
    "homeworld": {
      "name": "Kashyyyk",
      "rotation_period": "26",
      "orbital_period": "381",
      "diameter": "12765",
      "climate": "tropical",
      "gravity": "1 standard",
      "terrain": "jungle, forests, lakes, rivers",
      "surface_water": "60",
      "population": "45000000"
    },
    "species": [
      {
        "name": "Wookiee",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "210",
        "skin_colors": "gray",
        "hair_colors": "black, brown",
        "eye_colors": "blue, green, yellow, brown, golden, red",
        "average_lifespan": "400",
        "language": "Shyriiwook"
      }
    ],
    "vehicles": [
      {
        "name": "AT-ST",
        "model": "All Terrain Scout Transport",
        "manufacturer": "Kuat Drive Yards, Imperial Department of Military Research",
        "cost_in_credits": "unknown",
        "length": "2",
        "max_atmosphering_speed": "90",
        "crew": "2",
        "passengers": "0",
        "cargo_capacity": "200",
        "consumables": "none",
        "vehicle_class": "walker"
      }
    ]
  },
  {
    "name": "Han Solo",
    "hair_color": "brown",
    "skin_color": "fair",
    "eye_color": "brown",
    "birth_year": "29BBY",
    "gender": "male",
    "homeworld": {
      "name": "Corellia",
      "rotation_period": "25",
      "orbital_period": "329",
      "diameter": "11000",
      "climate": "temperate",
      "gravity": "1 standard",
      "terrain": "plains, urban, hills, forests",
      "surface_water": "70",
      "population": "3000000000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Greedo",
    "hair_color": "n/a",
    "skin_color": "green",
    "eye_color": "black",
    "birth_year": "44BBY",
    "gender": "male",
    "homeworld": {
      "name": "Rodia",
      "rotation_period": "29",
      "orbital_period": "305",
      "diameter": "7549",
      "climate": "hot",
      "gravity": "1 standard",
      "terrain": "jungles, oceans, urban, swamps",
      "surface_water": "60",
      "population": "1300000000"
    },
    "species": [
      {
        "name": "Rodian",
        "classification": "sentient",
        "designation": "reptilian",
        "average_height": "170",
        "skin_colors": "green, blue",
        "hair_colors": "n/a",
        "eye_colors": "black",
        "average_lifespan": "unknown",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Jabba Desilijic Tiure",
    "hair_color": "n/a",
    "skin_color": "green-tan, brown",
    "eye_color": "orange",
    "birth_year": "600BBY",
    "gender": "hermaphrodite",
    "homeworld": {
      "name": "Nal Hutta",
      "rotation_period": "87",
      "orbital_period": "413",
      "diameter": "12150",
      "climate": "temperate",
      "gravity": "1 standard",
      "terrain": "urban, oceans, swamps, bogs",
      "surface_water": "unknown",
      "population": "7000000000"
    },
    "species": [
      {
        "name": "Hutt",
        "classification": "gastropod",
        "designation": "sentient",
        "average_height": "300",
        "skin_colors": "green, brown, tan",
        "hair_colors": "n/a",
        "eye_colors": "yellow, red",
        "average_lifespan": "1000",
        "language": "Huttese"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Wedge Antilles",
    "hair_color": "brown",
    "skin_color": "fair",
    "eye_color": "hazel",
    "birth_year": "21BBY",
    "gender": "male",
    "homeworld": {
      "name": "Corellia",
      "rotation_period": "25",
      "orbital_period": "329",
      "diameter": "11000",
      "climate": "temperate",
      "gravity": "1 standard",
      "terrain": "plains, urban, hills, forests",
      "surface_water": "70",
      "population": "3000000000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": [
      {
        "name": "Snowspeeder",
        "model": "t-47 airspeeder",
        "manufacturer": "Incom corporation",
        "cost_in_credits": "unknown",
        "length": "4.5",
        "max_atmosphering_speed": "650",
        "crew": "2",
        "passengers": "0",
        "cargo_capacity": "10",
        "consumables": "none",
        "vehicle_class": "airspeeder"
      }
    ]
  },
  {
    "name": "Jek Tono Porkins",
    "hair_color": "brown",
    "skin_color": "fair",
    "eye_color": "blue",
    "birth_year": "unknown",
    "gender": "male",
    "homeworld": {
      "name": "Bestine IV",
      "rotation_period": "26",
      "orbital_period": "680",
      "diameter": "6400",
      "climate": "temperate",
      "gravity": "unknown",
      "terrain": "rocky islands, oceans",
      "surface_water": "98",
      "population": "62000000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Yoda",
    "hair_color": "white",
    "skin_color": "green",
    "eye_color": "brown",
    "birth_year": "896BBY",
    "gender": "male",
    "homeworld": {
      "name": "unknown",
      "rotation_period": "0",
      "orbital_period": "0",
      "diameter": "0",
      "climate": "unknown",
      "gravity": "unknown",
      "terrain": "unknown",
      "surface_water": "unknown",
      "population": "unknown"
    },
    "species": [
      {
        "name": "Yoda's species",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "66",
        "skin_colors": "green, yellow",
        "hair_colors": "brown, white",
        "eye_colors": "brown, green, yellow",
        "average_lifespan": "900",
        "language": "Galactic basic"
      }
    ],
    "vehicles": []
  },
  {
    "name": "Palpatine",
    "hair_color": "grey",
    "skin_color": "pale",
    "eye_color": "yellow",
    "birth_year": "82BBY",
    "gender": "male",
    "homeworld": {
      "name": "Naboo",
      "rotation_period": "26",
      "orbital_period": "312",
      "diameter": "12120",
      "climate": "temperate",
      "gravity": "1 standard",
      "terrain": "grassy hills, swamps, forests, mountains",
      "surface_water": "12",
      "population": "4500000000"
    },
    "species": [
      {
        "name": "Human",
        "classification": "mammal",
        "designation": "sentient",
        "average_height": "180",
        "skin_colors": "caucasian, black, asian, hispanic",
        "hair_colors": "blonde, brown, black, red",
        "eye_colors": "brown, blue, green, hazel, grey, amber",
        "average_lifespan": "120",
        "language": "Galactic Basic"
      }
    ],
    "vehicles": []
  }
]
